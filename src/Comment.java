public class Comment {

	private String text;
	private User author;
	private int likeNum;
	
	
	public Comment(String text, User author, int likeNum) {
		this.text = text;
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}
	
	public int getLikeNum() {
		return likeNum;
		
	}
	
	public void setLikeNum(int likeNum) {
		this.likeNum = likeNum;
	}
	
}
