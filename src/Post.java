import java.util.ArrayList;

public class Post {

	private int likeNumber;
	private User author;
	private String caption;
	private ArrayList<Image> imageList;
	private ArrayList<Comment> commentList;

	
	public Post(User author, String caption, ArrayList<Image> imageList, ArrayList<Comment> commentList) {
		
		this.author = author;
		this.caption = caption;
		this.imageList = imageList;
		this.commentList = commentList;
	}
	
	public String getCaption() {
		return caption;
		
	}
	
	public void getCaption(String caption) {
		this.caption = caption;
	}
	public ArrayList<Image> getImageList() {
		return imageList;
	}
	
	public ArrayList<Comment> getCommentList() {
		return commentList;
		
	}

	public int getLikeNumber() {
		return likeNumber;
	}
	
	public void setLikeNumber(int likeNumber) {
		this.likeNumber = likeNumber;
	}
	
	public User getAuthor() {
		return author;
	}
	
	public void setAuthor(User author) {
		this.author = author;
	}
	
	public int getCommentNumber() {
		return commentList.size();
	}
	
}
