
public class User {

	private String name;
	private String surname;
	private int followers;
	private int following;
	
	public User(String name, String surname, int followers, int following) {
		
		this.name = name;
		this.surname = surname;
		this.followers = followers;
		this.following = following;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}
	
	@Override
	public String toString() {
		return this.name + " " + this.surname;
	}
}
